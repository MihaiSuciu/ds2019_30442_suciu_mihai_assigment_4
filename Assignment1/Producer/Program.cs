﻿using System;
using RabbitMQ.Client;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading;
using Database.DAL;

namespace Producer
{
    class Program
    {
        public static void Main()
        {
            List<Activity> activities = new List<Activity>();
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader("activity.txt");
            while ((line = file.ReadLine()) != null)
            {
                //System.Console.WriteLine(line);
                string[] words = line.Split("		");
                activities.Add(new Activity
                {
                    StartTime = DateTime.Parse(words[0]),
                    EndTime = DateTime.Parse(words[1]),
                    ActivityLabel = words[2],
                    PacientID = 1

                }); ;
            }
            file.Close();

            

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello", durable: false, exclusive: false, autoDelete: false, arguments: null);

                for (int i = 0; i < activities.Count; i++)
                {
                    var json = JsonConvert.SerializeObject(activities[i]);
                    json.Replace('\t', ' ');
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: "", routingKey: "hello", basicProperties: null, body: body);
                    System.Console.WriteLine(" [x] Sent {0}", json);
                    //int milliseconds = 1000;
                    //Thread.Sleep(milliseconds);
                }
            }
            System.Console.WriteLine(" Press [enter] to exit.");
            System.Console.ReadLine();
        }
    }
}