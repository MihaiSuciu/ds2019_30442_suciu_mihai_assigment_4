﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Producer
{
    class Activity
    {
        public int ActivityID { get; set; }
        public int PacientID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ActivityLabel { get; set; }
    }
}
