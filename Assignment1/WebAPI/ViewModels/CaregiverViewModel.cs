﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.ViewModels
{
    public class CaregiverViewModel
    {
        public int CaregiverID { get; set; }
        public string Name { get; set; }
        public System.DateTime BirthDate { get; set; }
        public int Gender { get; set; }
        public string Address { get; set; }

        public CaregiverViewModel(Caregiver caregiver)
        {
            CaregiverID = caregiver.CaregiverID;
            Name = caregiver.Name;
            BirthDate = caregiver.BirthDate;
            Gender = caregiver.Gender;
            Address = caregiver.Address;
        }

    }
}