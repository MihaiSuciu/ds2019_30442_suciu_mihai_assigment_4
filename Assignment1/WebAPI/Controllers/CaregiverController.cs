﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;
using WebAPI.ViewModels;
using System.Web.Http.Cors;

namespace WebAPI.Controllers
{
    public class CaregiverController : ApiController
    {
        private HospitalEntities1 db = new HospitalEntities1();

        // GET: api/Caregiver
        public IEnumerable<Caregiver> GetCaregivers()
        {
            return db.Caregivers;
        }

        // GET: api/Caregiver/5
        [ResponseType(typeof(Caregiver))]
        public IHttpActionResult GetCaregiver(int id)
        {
            Caregiver caregiver = db.Caregivers.Find(id);
            if (caregiver == null)
            {
                return NotFound();
            }

            return Ok(caregiver);
        }

        // PUT: api/Caregiver/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCaregiver(int id, Caregiver caregiver)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != caregiver.CaregiverID)
            {
                return BadRequest();
            }

            db.Entry(caregiver).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CaregiverExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Caregiver
        [ResponseType(typeof(Caregiver))]
        public IHttpActionResult PostCaregiver(Caregiver caregiver)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Caregivers.Add(caregiver);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = caregiver.CaregiverID }, caregiver);
        }

        // DELETE: api/Caregiver/5
        [ResponseType(typeof(Caregiver))]
        public IHttpActionResult DeleteCaregiver(int id)
        {
            Caregiver caregiver = db.Caregivers.Find(id);
            if (caregiver == null)
            {
                return NotFound();
            }

            db.Caregivers.Remove(caregiver);
            db.SaveChanges();

            return Ok(caregiver);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CaregiverExists(int id)
        {
            return db.Caregivers.Count(e => e.CaregiverID == id) > 0;
        }
    }
}