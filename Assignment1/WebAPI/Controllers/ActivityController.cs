﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ActivityController : ApiController
    {

        [HttpGet]
        public IHttpActionResult Activities()
        {
            //Creating Web Service reference object  
            ActivitiesService.ActivitiesService activitiesService = new ActivitiesService.ActivitiesService();

            //calling and storing web service output into the variable  
            var activities = activitiesService.GetActivities().ToList();
            //returning josn result  
            return Ok(activities);

        }
    }
}