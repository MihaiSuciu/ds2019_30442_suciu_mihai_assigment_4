﻿using Database.DAL;
using Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Database.Controllers
{
    public class HomeController : Controller
    {
        DataContext db = new DataContext();
        public ActionResult Index()
        {
            Patient patient1 = new Patient { Name = "Andreea", BirthDate = DateTime.Parse("1994-01-08"), Gender = Gender.F, Address = "Cluj" };
            Patient patient2 = new Patient { Name = "Maria", BirthDate = DateTime.Parse("1998-07-25"), Gender = Gender.F, Address = "Baia Mare" };
            Patient patient3 = new Patient { Name = "Carmen", BirthDate = DateTime.Parse("1984-12-11"), Gender = Gender.F, Address = "Zalau" };
            Patient patient4 = new Patient { Name = "Raluca", BirthDate = DateTime.Parse("2001-06-17"), Gender = Gender.F, Address = "Constanta" };
            Patient patient5 = new Patient { Name = "Bianca", BirthDate = DateTime.Parse("2004-03-13"), Gender = Gender.F, Address = "Turda" };

            db.Patients.Add(patient1);
            db.Patients.Add(patient2);
            db.Patients.Add(patient3);
            db.Patients.Add(patient4);
            db.Patients.Add(patient5);

            Caregiver caregiver1 = new Caregiver { Name = "Mihai", BirthDate = DateTime.Parse("1997-09-11"), Gender = Gender.M, Address = "Cluj-Napoca" };
            Caregiver caregiver2 = new Caregiver { Name = "Alex", BirthDate = DateTime.Parse("2007-03-25"), Gender = Gender.M, Address = "Bucuresti" };
            Caregiver caregiver3 = new Caregiver { Name = "Dragos", BirthDate = DateTime.Parse("2002-07-21"), Gender = Gender.M, Address = "Iasi" };
            Caregiver caregiver4 = new Caregiver { Name = "Tudor", BirthDate = DateTime.Parse("1999-04-17"), Gender = Gender.M, Address = "Constanta" };

            db.Caregivers.Add(caregiver1);
            db.Caregivers.Add(caregiver2);
            db.Caregivers.Add(caregiver3);
            db.Caregivers.Add(caregiver4);

            Medication medication1 = new Medication { Name = "Aspirine", Dosage = 1, SideEffects = "Nu te mai doare capu" };
            Medication medication2 = new Medication { Name = "Parasinus", Dosage = 2, SideEffects = "Nu consuma alcool" };
            Medication medication3 = new Medication { Name = "Coldrex", Dosage = 3, SideEffects = "Antiraceala" };
            Medication medication4 = new Medication { Name = "Nurofen", Dosage = 3, SideEffects = "Doar pentru copii" };

            db.Medications.Add(medication1);
            db.Medications.Add(medication2);
            db.Medications.Add(medication3);
            db.Medications.Add(medication4);

            MedicationPlan medicationPlan1 = new MedicationPlan { Description = "Sa nu te mai imbolnavesti", Days = 4 };

            db.MedicationPlans.Add(medicationPlan1);

            Activity activity = new Activity();
            List<Activity> activities = new List<Activity>();
            activities= activity.getAllActivities("activity.txt");

            foreach(var act in activities)
            {
                db.Activities.Add(new Activity
                {
                    PacientID = act.PacientID,
                    ActivityLabel=act.ActivityLabel,
                    StartTime=act.StartTime,
                    EndTime=act.EndTime
                }) ;
            }
            db.SaveChanges();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}