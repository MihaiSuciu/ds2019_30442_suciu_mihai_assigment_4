﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Models
{
    public class Medication
    {
        public int MedicationID { get; set; }
        public string Name { get; set; }
        public string SideEffects { get; set; }
        public int Dosage { get; set; }
    }
}