﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Models
{
    public class Activity
    {
        public int ActivityID { get; set; }
        public int PacientID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ActivityLabel { get; set; }

        public List<Activity> getAllActivities(string filename)
        {
            List<Activity> activities = new List<Activity>();
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                System.Console.WriteLine(line);
                string[] words = line.Split(new string[] { "\t\t" }, StringSplitOptions.None);
                activities.Add(new Activity
                {
                    StartTime = DateTime.Parse(words[0]),
                    EndTime = DateTime.Parse(words[1]),
                    ActivityLabel = words[2],
                    PacientID = 1

                }); ;
            }
            file.Close();
            return activities;
        }
    }
}