﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Assignment4Soap
{
    /// <summary>
    /// Summary description for ActivitiesService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ActivitiesService : System.Web.Services.WebService
    {
        HospitalEntities db = new HospitalEntities();
        public void Add(Activity activity)
        {
            db.Activities.Add(activity);
        }
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public List<Activity> GetActivities()
        {
            return db.Activities.ToList();
        }
    }
}
