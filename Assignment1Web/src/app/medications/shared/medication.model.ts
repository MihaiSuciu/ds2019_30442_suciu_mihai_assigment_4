export class Medication {
    MedicationID:number;
    Name:string;
    SideEffects:string;
    Dosage:number;
}
