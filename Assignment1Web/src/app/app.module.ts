import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule} from '@angular/forms';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CaregiversComponent } from './caregivers/caregivers.component';
import { CaregiverComponent } from './caregivers/caregiver/caregiver.component';
import { CaregiverListComponent } from './caregivers/caregiver-list/caregiver-list.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HomepageComponent } from './homepage/homepage.component';
import { PatientsComponent } from './patients/patients.component';
import { MedicationsComponent } from './medications/medications.component';
import { DoctorPageComponent } from './doctor-page/doctor-page.component';
import { CaregiverPageComponent } from './caregiver-page/caregiver-page.component';
import { PatientPageComponent } from './patient-page/patient-page.component';
import { PatientComponent } from './patients/patient/patient.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { MedicationComponent } from './medications/medication/medication.component';
import { MedicationListComponent } from './medications/medication-list/medication-list.component';
import { PiechartComponent } from './doctor-page/piechart/piechart.component';

const appRoutes: Routes = [
  { path: 'app-homepage',component:HomepageComponent},
  { path: 'app-doctor-page',  component: DoctorPageComponent },
  { path: 'app-caregiver-page', component: CaregiverPageComponent },
  { path: 'app-patient-page',component:PatientPageComponent},
  { path: '',component:HomepageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CaregiversComponent,
    CaregiverComponent,
    CaregiverListComponent,
    HomepageComponent,
    PatientsComponent,
    MedicationsComponent,
    DoctorPageComponent,
    CaregiverPageComponent,
    PatientPageComponent,
    PatientComponent,
    PatientListComponent,
    MedicationComponent,
    MedicationListComponent,
    PiechartComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpModule, 
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
