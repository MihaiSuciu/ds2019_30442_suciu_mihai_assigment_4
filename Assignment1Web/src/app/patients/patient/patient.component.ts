import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'

import {PatientService} from '../shared/patient.service'
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  constructor(private patientService:PatientService,private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    if(form!=null){
      form.reset();
    }
    this.patientService.selectedPatient={
      PatientID:0,
      Name:'',
      BirthDate:null,
      Gender:null,
      Address:''
    }
  }
  onSubmit(form:NgForm){
    if(!form.value.PatientID){
      this.patientService.postPatient(form.value)
        .subscribe(data=>{
          this.resetForm(form);
          this.patientService.getPatientList();
          this.toastr.success('New record Added Successfully!','Patient Register');
        })
    }
    else{
      this.patientService.putPatient(form.value.PatientID,form.value)
        .subscribe(data=>{
          this.resetForm(form);
          this.patientService.getPatientList();
          this.toastr.info("Record Updated Successfully!","Patient Register");
        })
    }
  }

}
