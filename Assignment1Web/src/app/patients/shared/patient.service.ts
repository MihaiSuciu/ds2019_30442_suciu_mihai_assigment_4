import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Patient} from './patient.model'
@Injectable({
  providedIn: 'root'
})
export class PatientService {
  selectedPatient:Patient;
  patientList:Patient[];
  constructor(private http : Http) { }

  postPatient(patient : Patient){
    var body = JSON.stringify(patient);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post('http://localhost:44341/api/Patient',body,requestOptions).map(x => x.json());
  }
  putPatient(id, patient) {
    var body = JSON.stringify(patient);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:44341/api/Patient/' + id,
      body,
      requestOptions).map(res => res.json());
  }
  getPatientList(){
    this.http.get('http://localhost:44341/api/Patient')
    .map((data : Response) =>{
      return data.json() as Patient[];
    }).toPromise().then(x => {
      this.patientList = x;
    })
  }
  deletePatient(id: number) {
    return this.http.delete('http://localhost:44341/api/Patient/' + id).map(res => res.json());
  }
}
