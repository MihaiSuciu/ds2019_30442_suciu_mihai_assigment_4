import { Component, OnInit } from '@angular/core';
import * as CanvasJS from 'src/assets/canvasjs.min.js';
import { ActivityService } from '../shared/activity.service';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.css']
})
export class PiechartComponent implements OnInit {

  constructor(private activityService: ActivityService) { }

  ngOnInit() {
    // let chart = new CanvasJS.Chart("chartContainer", {
    //   theme: "light2",
    //   animationEnabled: true,
    //   exportEnabled: true,
    //   title:{
    //     text: "Daily Activity"
    //   },
    //   data: [{
    //     type: "pie",
    //     showInLegend: true,
    //     toolTipContent: "<b>{name}</b>: {y} (#percent%)",
    //     indexLabel: "{name} - #percent%",
    //     dataPoints: [
    //       { y: 450, name: "Sleeping" },
    //       { y: 120, name: "Toileting" },
    //       { y: 300, name: "Showering" },
    //       { y: 800, name: "Breakfast" },
    //       { y: 150, name: "Grooming" },
    //       { y: 150, name: "Spare_Time/TV"},
    //       { y: 150, name: "Leaving" },
    //       { y: 100, name: "Lunch" },
    //       { y: 100, name: "Snack" }
    //     ]
    //   }]
    // });
      
    // chart.render();
  }

  onChange(date: Date){
    this.activityService.getActivityList().subscribe(result=>{
      var items = result.filter(item => {
        var itemDate = new Date(item.startTimeField);
        return new Date(itemDate.getFullYear(), itemDate.getMonth(), itemDate.getDate()).getTime() == 
                new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime()
      });
      var hash = items.reduce((hash, item)=> {
        if(!hash[item.activityLabelField.trim()])
          hash[item.activityLabelField.trim()] = 0;
        hash[item.activityLabelField.trim()]++;
        return hash;
      }, {});
      console.log(hash)
       let chart = new CanvasJS.Chart("chartContainer", {
          theme: "light2",
          animationEnabled: true,
          exportEnabled: true,
          title:{
            text: "Daily Activity"
          },
          data: [{
            type: "pie",
            showInLegend: true,
            toolTipContent: "<b>{name}</b>: {y} (#percent%)",
            indexLabel: "{name} - #percent%",
            dataPoints: Object.keys(hash).map(key => ({y : hash[key], name: key}))
          }]
    });
      
    chart.render();
    });
  }

  process(d){
    var parts = d.split("-");
    parts[2] = parts[2].slice(0, 2)
    var date = new Date(parts[0] , parts[1] , parts[2]);
    return date.getTime();
 }

}
