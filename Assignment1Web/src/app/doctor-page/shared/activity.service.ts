import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Activity} from './activity.model'
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  constructor(private http : Http) { }

  getActivityList() : Observable<any>{
    return this.http.get('http://localhost:44341/api/Activity').map((data : Response) =>{
      return data.json();
    });
  }
}
