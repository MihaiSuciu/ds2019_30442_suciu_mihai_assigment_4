export class Activity {
    ActivityID:number;
    PacientID:number;
    StartTime:Date;
    EndTime:Date;
    ActivityLabel:string;
}
