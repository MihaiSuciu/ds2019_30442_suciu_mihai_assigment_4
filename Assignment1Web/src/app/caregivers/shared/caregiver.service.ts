import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Caregiver} from './caregiver.model'
@Injectable({
  providedIn: 'root'
})
export class CaregiverService {
  selectedCaregiver:Caregiver;
  caregiverList:Caregiver[];
  constructor(private http : Http) { }

  postCaregiver(caregiver : Caregiver){ 
    var body = JSON.stringify(caregiver);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post('http://localhost:44341/api/Caregiver',body,requestOptions).map(x => x.json());
  }
  putCaregiver(id, caregiver) {
    var body = JSON.stringify(caregiver);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:44341/api/Caregiver/' + id,
      body,
      requestOptions).map(res => res.json());
  }
  getCaregiverList(){
    this.http.get('http://localhost:44341/api/Caregiver')
    .map((data : Response) =>{
      return data.json() as Caregiver[];
    }).toPromise().then(x => {
      this.caregiverList = x;
    })
  }
  deleteCaregiver(id: number) {
    return this.http.delete('http://localhost:44341/api/Caregiver/' + id).map(res => res.json());
  }
}
