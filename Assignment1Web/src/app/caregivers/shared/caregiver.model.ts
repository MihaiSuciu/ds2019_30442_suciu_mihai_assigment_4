import { Patient } from 'src/app/patients/shared/patient.model';

export class Caregiver {
    CaregiverID:number;
    Name:string;
    BirthDate:Date;
    Gender:number;
    Address:string;
}
